/*configuracion necesaria*/
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var obNotificacion=require('./routes/index.js');
global.config=require('./config.json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var server=app.listen(global.config.puerto, function () {
  var port = server.address().port
  console.log("Escuchando en puerto: "+port);
});
/*Fin configuracion*/

/*Peticiones REST*/
app.get('/correo',function(req,res){
  obNotificacion.enviarCorreo(function(page){
      res.send(page);
  });
});
